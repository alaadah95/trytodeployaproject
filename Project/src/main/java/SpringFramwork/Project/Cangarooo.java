package SpringFramwork.Project;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Cangarooo implements AnimalBussiness {

	private DrinkBussiness water;

	public Cangarooo(@Qualifier("watterBean") DrinkBussiness water) {

		this.water = water;
	}

	public String name() {

		return "Hi Iam Cangaroooooo";
	}

	public boolean isIntelligent() {

		return false;
	}

	public int averageAge() {

		return 15;
	}

	public String isDrinkWatter() {

		return water.getNaturalWatter();
	}

	public void onstart() {
		System.out.println("onstart");
	}

	public void ondestroy() {
		System.out.println("ondestroy");
	}
}
