package SpringFramwork.Project;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;

//@Component

public class Monkey implements AnimalBussiness {
	
	@Value("${m.email}")
	private String defaulVal ; 
	
	private DrinkBussiness water;
	

	public Monkey(DrinkBussiness water) {

		this.water = water;
	}

	public String name() {
		return "Hi I am Monkey .  ";
	}

	public boolean isIntelligent() {
		return true;
	}

	public int averageAge() {

		return 25;
	}

	public String isDrinkWatter() {
		 
		return water.getNaturalWatter();
	}
	
	public void onstart() {
		System.out.println("onstart");
	}

	public void ondestroy() {
		System.out.println("ondestroy");
	}

	public String getDefaulVal() {
		return defaulVal;
	}

	public void setDefaulVal(String defaulVal) {
		this.defaulVal = defaulVal;
	}

	public DrinkBussiness getWater() {
		return water;
	}

	public void setWater(DrinkBussiness water) {
		this.water = water;
	}

	
}
