package SpringFramwork.Project;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Cow implements AnimalBussiness {

	private DrinkBussiness water;

	public Cow(DrinkBussiness water) {
		System.out.println("Cow Constructor");
		this.water = water;
	}

	public String name() {

		return "Hi Iam Cow";
	}

	public boolean isIntelligent() {

		return false;
	}

	public int averageAge() {

		return 15;
	}

	public String isDrinkWatter() {

		return water.getNaturalWatter();
	}

	public void onstart() {
		System.out.println("onstart");
	}

	public void ondestroy() {
		System.out.println("ondestroy");
	}

}
