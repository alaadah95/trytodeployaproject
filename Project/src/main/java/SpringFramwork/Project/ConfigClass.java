package SpringFramwork.Project;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan("SpringFramwork.Project")
@PropertySource("classpath:app.properties")
public class ConfigClass {
	
	
	@Bean
	public DrinkBussiness watterBean() {
		return new Watter();
	}
	
	@Bean
	@Scope("prototype")
	public Monkey monkeyBean() {
		return new Monkey(watterBean());
	}
}
