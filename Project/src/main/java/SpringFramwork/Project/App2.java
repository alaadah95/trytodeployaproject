package SpringFramwork.Project;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import SpringFramwork.Project.Entity.Course;
import SpringFramwork.Project.Entity.Instructor;
import SpringFramwork.Project.Entity.InstructorDetails;
import SpringFramwork.Project.Entity.Review;
import SpringFramwork.Project.Entity.Student;

public class App2 {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(InstructorDetails.class).addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(Course.class).addAnnotatedClass(Review.class)
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();

		Session session = factory.getCurrentSession();
		session.beginTransaction();

		// InstructorDetails detials = new InstructorDetails();
		// detials.setHobby("hobby");
		// detials.setYoutube("youtku@sdssdsd.conk");

		//
		// Instructor instructor =
		// Instructor.builder().withEmail("Mazika@MMMAAAA.com").withFirstName("ddaw")
		// .withLastName("NAzsss").withInstructorDetailsId(detials).build();

		// Instructor instructor = session.get(Instructor.class, 7);

		// Course course = Course.builder().withTitle("PYTHON COURSE").build();
		// course.setInstructor(instructor);
		//
		// Course course2 = Course.builder().withTitle("JAVAAA COURSE").build();
		// course2.setInstructor(instructor);

		// Instructor ins = session.get(Instructor.class, 6);
		// Course course = Course.builder().withTitle("JAVA
		// COURSE").withInstructor(ins).build();
		//
		// // instructor.setFirstName("Alaa");
		// // instructor.setLastName("Daher");
		// // instructor.setEmail("email@0123.com");
		// // instructor.setInstructorDetailsId(detials);
		//
		// session.save(course);
		// session.save(course2);

		//
		//
		// System.out.println(details);
		// System.out.println(instructor.getCourses().size());

		// Query<Instructor> query = session.createQuery(
		// "select i from Instructor i join fetch i.courses where i.id = :instId ",
		// Instructor.class);
		// query.setParameter("instId", 7);
		// Instructor inst = query.getSingleResult();

	
		Course course = session.get(Course.class, 9);
		
	
		
	
		
		Student student = new Student();
		student.setFirstName("Alaa1188881");
		student.setLastName("Mahmoud222");
		student.setEmail("alaa@google.com");
		session.save(student);
		
		 
		
		course.addstudents(student);
		
		Serializable course2 =  session.save(course);
		
	
		
		session.getTransaction().commit();
		session.close();
		factory.close();

	}

}
