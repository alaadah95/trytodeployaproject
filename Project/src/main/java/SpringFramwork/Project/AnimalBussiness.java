package SpringFramwork.Project;

public interface AnimalBussiness {

	String name();

	boolean isIntelligent();

	int averageAge();

	String isDrinkWatter();

}
