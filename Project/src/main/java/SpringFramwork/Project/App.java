package SpringFramwork.Project;

import java.text.SimpleDateFormat;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import SpringFramwork.Project.Entity.Student;

public class App {
	public static void main(String[] args) {

		// ClassPathXmlApplicationContext applicationContext = new
		// ClassPathXmlApplicationContext(
		// "applicationContext.xml");
		//
		// AnnotationConfigApplicationContext applicationContext = new
		// AnnotationConfigApplicationContext(
		// ConfigClass.class);
		// Monkey cowInstance = applicationContext.getBean("monkeyBean", Monkey.class);
		// System.out.println(cowInstance.name());
		// System.out.println(cowInstance.isIntelligent());
		// System.out.println(cowInstance.averageAge());
		// System.out.println(cowInstance.isDrinkWatter());
		// System.out.println(cowInstance.getDefaulVal());
		//
		// AnimalBussiness cowInstance2 = applicationContext.getBean("monkeyBean",
		// AnimalBussiness.class);
		// System.out.println(cowInstance == cowInstance2);

		// AnimalBussiness instance = applicationContext.getBean("cangarooBean",
		// AnimalBussiness.class);
		// System.out.println(instance.name());
		// System.out.println(instance.isIntelligent());
		// System.out.println(instance.averageAge());
		// System.out.println(instance.isDrinkWatter());

		// applicationContext.close();

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();

//			Student student = new Student();
//			student.setFirstName("datatat");
//			student.setLastName("Mahm");
//			student.setEmail("ssss@Email.com");
//			student.setBirthDate(new Date());
//			session.save(student);
//			List<Student> list = session.createQuery("from Student").getResultList();
//					
//			for (Student s : list) {
//				System.out.println(s);
//			}
//			
//			session.getTransaction().commit();
//			
//			session = factory.getCurrentSession();
//			session.beginTransaction();
//			
//		
//			List<Student> list2= session.createQuery("from Student s where s.firstName = '"+"alaa".toUpperCase()+"'").getResultList();
//			for (Student s : list2) {
//				System.out.println(s);
//			}
			
			
			
			Student studentclass = 	session.get(Student.class, 6);
//			studentclass.setFirstName("lolo");
//			session.delete(studentclass);
			
			SimpleDateFormat  formater = new SimpleDateFormat("dd/MM/yyyy");
//			String date = formater.format(studentclass.getBirthDate());
			
//			System.out.println(date);
//			session.createQuery("update Student s set  s.email = '"+"lolo@Gmil.com".toUpperCase()+"'").executeUpdate();
			session.getTransaction().commit();
			
			
			
			
		} finally {
			factory.close();

		}
	}
}
